//
//  IResponse.swift
//  pruebapicap
//
//  Created by Pablo Linares on 10/19/18.
//  Copyright © 2018 Pablo Linares. All rights reserved.
//

import Foundation

//this protocol is listener of called for proxy in request services
protocol IResponse {
    /**
     Notify a succesfull response with a model object as data source
     response. To receive this response must be any type of IBaseModel
     (usually a child of BaseModel class).
     
     - parameter response: Data source response. This must be any child of BaseModel.
     */
    func onDataResponse<T: Codable>(_ response: T, tag: Constants.RepositoriesTags)
    
    /**
     Notify a failed response with a response model. This model contains
     the error code status and message.
     
     - parameter response: Response model with error code status and message.
     */
    func onFailedResponse(_ message: String, tag: Constants.RepositoriesTags)
    
}
