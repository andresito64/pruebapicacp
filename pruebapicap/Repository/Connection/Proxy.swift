//
//  Proxy.swift
//  pruebapicap
//
//  Created by Pablo Linares on 10/19/18.
//  Copyright © 2018 Pablo Linares. All rights reserved.
//

import Foundation
import Alamofire
func isNetworkAvaible() -> Bool {
    return NetworkReachabilityManager()!.isReachable
}

class Proxy<T: Codable> {
    
    
    // execute function called web service
    static func execute(_ method: Alamofire.HTTPMethod,
                        url: String,
                        listener: IResponse,
                        tag: Constants.RepositoriesTags) {
        
        if !isNetworkAvaible() {
            listener.onFailedResponse(Constants.Dialogs.NOT_INTERNET_CONNECTION, tag: tag)
            return
        }
        let headers = [String: String]()
        
        do{
            try Alamofire.request(url.asURL(),
                                  method: method,
                                  parameters: nil,
                                  encoding: JSONEncoding.default,
                                  headers: headers).responseJSON{ response in
                                    processResponse(response, listener: listener, tag: tag)
            }
        }
        catch {
            listener.onFailedResponse(Constants.Dialogs.ERROR_REQUEST, tag: tag)
        }
    }
    
    
    //process response of server
    
    fileprivate static func processResponse(_ response: Alamofire.DataResponse<Any>, listener: IResponse, tag: Constants.RepositoriesTags) {
        let httpError = response.result.error
        let codeStatuts = response.response?.statusCode
        let dictionary = response.result.value as? [String: AnyObject]
        let arrayDictionary = response.result.value as? [[String: Any]]
        
        if httpError?.localizedDescription != nil {
            if codeStatuts == 200{
                let notInternet = Constants.RepositoriesTags.NOT_INTERNET_CONECTION
                listener.onFailedResponse(Constants.Dialogs.EMPTY_RESPONSE, tag: notInternet)
            }
            else{
                listener.onFailedResponse(Constants.Dialogs.INVALID_SERVER_RESPONSE, tag: tag)
            }
            return
        }
        
        // An error has ocurred
        if codeStatuts != 200 {
            listener.onFailedResponse(Constants.Dialogs.ERROR_WITH_CODE + (codeStatuts?.description)!, tag: tag)
            return
        }
        
        //Convert json information in to object codable
        if dictionary != nil{
            do{
                var array : [[String: AnyObject]] = []
                array.append(dictionary!)
                let data = try JSONSerialization.data(withJSONObject: array, options: [])
                let object = try JSONDecoder().decode(T.self, from: data)
                listener.onDataResponse(object, tag: tag)
            }
            catch{
                listener.onFailedResponse(error.localizedDescription, tag: tag)
            }
        }
        if arrayDictionary != nil {
            do{
                let data = try JSONSerialization.data(withJSONObject: arrayDictionary!, options: [])
                let object = try JSONDecoder().decode(T.self, from: data)
                listener.onDataResponse(object, tag: tag)
            }
            catch{
                listener.onFailedResponse(error.localizedDescription, tag: tag)
            }
        }
        else{
                    listener.onFailedResponse(Constants.Dialogs.NOT_VALID_DATA, tag: tag)
        }
    }
}


