//
//  RestService.swift
//  pruebapicap
//
//  Created by Pablo Linares on 10/19/18.
//  Copyright © 2018 Pablo Linares. All rights reserved.
//

import Foundation
class RestService {
    //get list of services
    func getServices(_ listener: IResponse, number: Int?){
        var url = Constants.Connection.GET_SERVICE
        if number != nil {
            url += "?page=\(number!.description)&\(Constants.Key.TOKEN)"
        }
        else {
            url += "?\(Constants.Key.TOKEN)"
        }
        Proxy<Service>.execute(.get,
                               url: url,
                               listener: listener,
                               tag: .GET_SERVICE)
    }
    //get detail of service by id
    func getDetailService(_ listener: IResponse, idService: String){
        let url = Constants.Connection.GET_DATAIL_SERVICE + idService + "?" + Constants.Key.TOKEN
        Proxy<Service>.execute(.get,
                               url: url,
                               listener: listener,
                               tag: .GET_DATAIL_SERVICE)
    }
}
