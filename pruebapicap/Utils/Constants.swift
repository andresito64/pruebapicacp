//
//  Constants.swift
//  pruebapicap
//
//  Created by Pablo Linares on 10/19/18.
//  Copyright © 2018 Pablo Linares. All rights reserved.
//

import Foundation
struct Constants {
    enum RepositoriesTags {
        case NOT_INTERNET_CONECTION
        case GET_SERVICE
        case GET_DATAIL_SERVICE
    }
    
    struct Connection {
        static let GET_SERVICE = "http://picapst.herokuapp.com/api/pssg/bookings"
        static let GET_DATAIL_SERVICE = "http://picapst.herokuapp.com/api/pssg/bookings/"
    }
    
    struct Key {
        static let TOKEN = "t=4DsKmPxzcqJ00IUUEvcdGTmbex3e105pd2NxmraT5QZ3-Fwj4dSQIg"
        static let DETAIL_CONTROLLER = "DetailServiceViewController"
    }
    
    struct Cell {
        static let CELL = "ServiceCell"
    }
    
    struct Dialogs {
        static let NOT_INTERNET_CONNECTION = "El Dispositivo no tiene acceso a internet"
        static let INVALID_SERVER_RESPONSE = "Respuesta invalida del servidor"
        static let EMPTY_RESPONSE = "Respuesta vacia del servidor"
        static let ERROR_WITH_CODE = "Error de servidor codigo = "
        static let ERROR_REQUEST = "Error al generar la peticion al servidor"
        static let NOT_VALID_DATA = "No se pudo interpretar respuesta del servidor"
        static let SEARCH_DRIVER = "Buscando conductor"
        static let DRIVER_IN_ROUTE = "Conductor en camino"
        static let WAITING_FOR_PASSENGER = "Esperando al pasajero"
        static let PASSENGER_ON_BOARD = "Pasajero en camino"
        static let END_TO_DRIVER = "Finalizado por conductor"
        static let CANCEL_TO_DRIVER = "Cancelado por conductor"
        static let CANCEL_FOR_PASSENGER = "Cancelado por pasajero"
        static let EXPIRED = "Expirado"
        static let ADDRESS = "Dirección: "
        static let STATE_OF_SERVICE = "Estado del servicio = "
   }
    
    struct Images {
        static let AVATAR = "avatar"
    }
}
