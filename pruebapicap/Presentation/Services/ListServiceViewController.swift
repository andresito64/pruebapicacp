//
//  ViewController.swift
//  pruebapicap
//
//  Created by Pablo Linares on 10/19/18.
//  Copyright © 2018 Pablo Linares. All rights reserved.
//

import UIKit
import JTProgressHUD

class ListServiceViewController: UITableViewController, IResponse {
    
    fileprivate var listService: [[ServiceElement]] = []
    fileprivate var countPages = 1
    fileprivate var isFinish = false
    fileprivate var isCalled = true

    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.async {
            JTProgressHUD.show()
        }
        DispatchQueue.global().async {
            RestService().getServices(self, number: nil)
        }
        
    }

    //return number of rows  in this one seccion
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listService[section].count
    }

    
    //return prototype cell ServiceCell
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.Cell.CELL, for: indexPath) as! ServiceCell
        cell.setCell(service: listService[indexPath.section][indexPath.row])
        cell.backgroundColor = (indexPath.row % 2 == 0 ? UIColor.white : UIColor.lightGray)
        return cell
    }
    
    //return number of seccions in this case number of pages
    override func numberOfSections(in tableView: UITableView) -> Int {
        return listService.count
    }
    
    
    //setView to header section indicate to number page
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 40))
        label.text = "Pagina \(section + 1)"
        label.textAlignment = .center
        label.backgroundColor = .black
        label.textColor = .white
        return label
    }
    
    
    
    //listener to end scroll the actual data, this call service web
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let  height = scrollView.frame.size.height
        let contentYoffset = scrollView.contentOffset.y
        let distanceFromBottom = scrollView.contentSize.height - contentYoffset
        if distanceFromBottom < height  && !isFinish && !isCalled{
            isCalled = true
            DispatchQueue.main.async {
                JTProgressHUD.show()
            }
            DispatchQueue.global().async {
                RestService().getServices(self, number: self.countPages)
            }
            countPages += 1
        }

    }
    
    
    //delegate to called in touch row, navigate to detail service
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let controller = self.storyboard?.instantiateViewController(withIdentifier: Constants.Key.DETAIL_CONTROLLER) as! DetailServiceViewController
        controller.idService = listService[indexPath.section][indexPath.row].id
        DispatchQueue.main.async {
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }


    
    //delegates functions of server
    func onDataResponse<T>(_ response: T, tag: Constants.RepositoriesTags) where T : Decodable, T : Encodable {
        switch tag {
        case .GET_SERVICE:
            if let array  = response as? [ServiceElement]{
                if array.count == 0{
                    isFinish = true
                    return
                }
                listService.append(array)
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                    self.isCalled = false
                    JTProgressHUD.hide()
                }
            }
            
            break
        default:
            break
        }
    }
    
    func onFailedResponse(_ message: String, tag: Constants.RepositoriesTags) {
        DispatchQueue.main.async {
            JTProgressHUD.hide()
        }
        isCalled = false
    }
}

