//
//  ServiceCell.swift
//  pruebapicap
//
//  Created by Pablo Linares on 10/19/18.
//  Copyright © 2018 Pablo Linares. All rights reserved.
//

import UIKit

class ServiceCell: UITableViewCell {
    @IBOutlet weak var viewState: UIView!
    @IBOutlet weak var labelState: UILabel!
    @IBOutlet weak var labelAddress: UILabel!
    
    func setCell(service: ServiceElement)  {
        var text = Constants.Dialogs.STATE_OF_SERVICE
        switch service.statusCD {
            case 0:
                text += Constants.Dialogs.SEARCH_DRIVER
                viewState.backgroundColor = .yellow
                break
            case 1:
                text += Constants.Dialogs.DRIVER_IN_ROUTE
                viewState.backgroundColor = .orange
                break
            case 2:
                text += Constants.Dialogs.WAITING_FOR_PASSENGER
                viewState.backgroundColor = .blue
                break
            case 3:
                text += Constants.Dialogs.PASSENGER_ON_BOARD
                viewState.backgroundColor = .green
                break
            case 4:
                text += Constants.Dialogs.END_TO_DRIVER
                viewState.backgroundColor = .green
                break
            case 100:
                text += Constants.Dialogs.CANCEL_TO_DRIVER
                viewState.backgroundColor = .red
                break
            case 102:
                text += Constants.Dialogs.CANCEL_FOR_PASSENGER
                viewState.backgroundColor = .red
                break
            case 101:
                text += Constants.Dialogs.EXPIRED
                viewState.backgroundColor = .black
                break
            default:
                break
        }
        labelState.text = text
        labelAddress.text = Constants.Dialogs.ADDRESS + service.address!
    }
}
