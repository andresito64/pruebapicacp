//
//  DetailServiceViewController.swift
//  pruebapicap
//
//  Created by Pablo Linares on 10/19/18.
//  Copyright © 2018 Pablo Linares. All rights reserved.
//

import UIKit
import JTProgressHUD
import Kingfisher

class DetailServiceViewController: UIViewController, IResponse {
    var idService: String!
    @IBOutlet weak var viewState: UIView!
    @IBOutlet weak var labelState: UILabel!
    @IBOutlet weak var imageDriver: UIImageView!
    @IBOutlet weak var imageUser: UIImageView!
    @IBOutlet weak var labelNameDriver: UILabel!
    @IBOutlet weak var labelNameUser: UILabel!
    @IBOutlet weak var labelAddress: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.async {
            JTProgressHUD.show()
        }
        DispatchQueue.global().async {
            RestService().getDetailService(self, idService: self.idService)
        }
        // Do any additional setup after loading the view.
    }
    
    
    // private func set views with server response ok
    fileprivate func setView(_ service: ServiceElement){
        var text = Constants.Dialogs.STATE_OF_SERVICE
        switch service.statusCD {
        case 0:
            text += Constants.Dialogs.SEARCH_DRIVER
            viewState.backgroundColor = .yellow
            break
        case 1:
            text += Constants.Dialogs.DRIVER_IN_ROUTE
            viewState.backgroundColor = .orange
            break
        case 2:
            text += Constants.Dialogs.WAITING_FOR_PASSENGER
            viewState.backgroundColor = .blue
            break
        case 3:
            text += Constants.Dialogs.PASSENGER_ON_BOARD
            viewState.backgroundColor = .green
            break
        case 4:
            text += Constants.Dialogs.END_TO_DRIVER
            viewState.backgroundColor = .green
            break
        case 100:
            text += Constants.Dialogs.CANCEL_TO_DRIVER
            viewState.backgroundColor = .red
            break
        case 102:
            text += Constants.Dialogs.CANCEL_FOR_PASSENGER
            viewState.backgroundColor = .red
            break
        case 101:
            text += Constants.Dialogs.EXPIRED
            viewState.backgroundColor = .black
            break
        default:
            break
        }
        labelState.text = text
        if let driver = service.driver{
            imageDriver.kf.setImage(with: URL(string: driver.photoURL!), placeholder: UIImage(named: Constants.Images.AVATAR))
            labelNameDriver.text = driver.name
            imageDriver.setCorner(imageDriver.frame.height / 2)
        }
        if let user = service.passenger{
            imageUser.kf.setImage(with: URL(string: user.photoURL!), placeholder: UIImage(named: Constants.Images.AVATAR))
            labelNameUser.text = user.name
            imageUser.setCorner(imageUser.frame.height / 2)
        }
        labelAddress.text = Constants.Dialogs.ADDRESS + service.address!
    }
    
    
    
    //delegates of called server
    func onDataResponse<T>(_ response: T, tag: Constants.RepositoriesTags) where T : Decodable, T : Encodable {
        if let service = response as? [ServiceElement]{
            DispatchQueue.main.async {
                self.setView(service.first!)
                JTProgressHUD.hide()
            }
        }
    }
    
    func onFailedResponse(_ message: String, tag: Constants.RepositoriesTags) {
        
    }

}


extension UIView{
    func setCorner(_ cornerRadius: CGFloat){
        self.layoutIfNeeded()
        self.layer.cornerRadius = cornerRadius
        self.layer.masksToBounds = true
    }
}
