//
//  Service.swift
//  pruebapicap
//
//  Created by Pablo Linares on 10/19/18.
//  Copyright © 2018 Pablo Linares. All rights reserved.
//

import Foundation

typealias Service = [ServiceElement]

class ServiceElement: Codable {
    let id, address: String?
    let amountChargedToPassengerWallet, amountChargedToPaymentMethod, amountPaidToDriverWallet, amountToChargeInCash: PromoBalance?
    let bookingForCD: Int?
    let endAddress: String?
    let endGeojson: endGeoJson?
    let finalCost: PromoBalance?
    let originGeojson: Geojson?
    let paymentMethodCD: Int?
    let rendAddress, rendAt: String?
    let rendGeojson: Geojson?
    let statusCD: Int?
    let trvDistanceStr: String?
    let trvTimeStr: String?
    let driver: Driver?
    let passenger: Driver?
    let servedVehicle: ServedVehicle?
    let requestedServiceType: RequestedServiceType?
    let finalCostCents: Int?
    let secondsToExpire: Int?
    let createdAt: String?
    let additionalPassengerParams: AdditionalPassengerParams?
    let relaunchedTo: ServiceElement?
    
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case address
        case amountChargedToPassengerWallet = "amount_charged_to_passenger_wallet"
        case amountChargedToPaymentMethod = "amount_charged_to_payment_method"
        case amountPaidToDriverWallet = "amount_paid_to_driver_wallet"
        case amountToChargeInCash = "amount_to_charge_in_cash"
        case bookingForCD = "booking_for_cd"
        case endAddress = "end_address"
        case endGeojson = "end_geojson"
        case finalCost = "final_cost"
        case originGeojson = "origin_geojson"
        case paymentMethodCD = "payment_method_cd"
        case rendAddress = "rend_address"
        case rendAt = "rend_at"
        case rendGeojson = "rend_geojson"
        case statusCD = "status_cd"
        case trvDistanceStr = "trv_distance_str"
        case trvTimeStr = "trv_time_str"
        case driver, passenger
        case servedVehicle = "served_vehicle"
        case requestedServiceType = "requested_service_type"
        case finalCostCents = "final_cost_cents"
        case secondsToExpire = "seconds_to_expire"
        case createdAt = "created_at"
        case additionalPassengerParams = "additional_passenger_params"
        case relaunchedTo = "relaunched_to"
    }
    
}

class AdditionalPassengerParams: Codable {
    let promoBalance: PromoBalance
    
    enum CodingKeys: String, CodingKey {
        case promoBalance = "promo_balance"
    }
}

class PromoBalance: Codable {
    let subunits: Int?
    let iso: String?
}

class endGeoJson: Codable {
    let type: String?
    let coordinates: [Double]?
    
}


class Driver: Codable {
    let id: String?
    let name: String?
    let phone: String?
    let ratingAsDriver: Int?
    let photoURL: String?
    let ratingAsPassenger: Int?
    
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case name, phone
        case ratingAsDriver = "rating_as_driver"
        case photoURL = "photo_url"
        case ratingAsPassenger = "rating_as_passenger"
    }
    
}


class Geojson: Codable {
    let type: String?
    let coordinates: [Double]?
    
    init(type: String?, coordinates: [Double]?) {
        self.type = type
        self.coordinates = coordinates
    }
}

class RequestedServiceType: Codable {
    let id: String?
    let name: String?
    
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case name
    }
    
}


class ServedVehicle: Codable {
    let id: String?
    let locCourse: Int?
    let locGeojson: Geojson?
    let plates: String?
    let color: String?
    let make: String?
    let reference: String?
    let markerIcon: String?
    let vehicleType: VehicleType?
    
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case locCourse = "loc_course"
        case locGeojson = "loc_geojson"
        case plates, color, make, reference
        case markerIcon = "marker_icon"
        case vehicleType = "vehicle_type"
    }
}

class VehicleType: Codable {
    let id: String?
    let etaX: Double?
    
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case etaX = "eta_x"
    }
}

